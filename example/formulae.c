#include <math.h>
#include <stdlib.h>

double *quadratic(double a, double b, double c)
{
	double discriminant = b*b - 4*a*c;

	if (discriminant < 0) return NULL;

	double *res = malloc(2 * sizeof(double));

	res[0] = (-b + sqrt(discriminant)) / (2*a);
	res[1] = (-b - sqrt(discriminant)) / (2*a);

	return res;
}
