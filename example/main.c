#include <stdlib.h>
#include <sysexits.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "formulae.h"

void usage(char *arg0)
{
	fprintf(stderr, "Usage: %s a b c\n", arg0);
	fprintf(stderr, "where a, b and c are real numbers.\n");
}

int main(int argc, char *argv[])
{
	double coefficients[3];
	double *roots;
	int i;

	if (argc != 4) {
		usage(argv[0]);
		return EX_USAGE;
	}

	for (i=0; i < 3; i++) {
		errno = 0;
		coefficients[i] = strtod(argv[i+1], NULL);
		if (errno != 0) {
			fprintf(stderr, "%s is not a real number: %s\n",
					argv[i+1], strerror(errno));
			usage(argv[0]);
			return EX_USAGE;
		}
	}

	roots = quadratic(coefficients[0], coefficients[1], coefficients[2]);

	if (roots) {
		printf("%f\n%f\n", roots[0], roots[1]);
		free(roots);
	} else {
		fprintf(stderr, "%fx^2 + %fx + %f has no real roots\n",
				coefficients[0],
				coefficients[1],
				coefficients[2]);
		return EX_DATAERR;
	}

	return EX_OK;
}
