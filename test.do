# Build everything that we can build.
redo example/all tests/all

# Test that the example configure script works. The configuration will already
# have been built above, but we can build it again.
(cd example; rm -f config; ./configure) >/dev/null
