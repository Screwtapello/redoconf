# Redo all the tests in this directory.
# We use redo rather than redo-ifchange to avoid caching.
redo $(echo test_*.do | sed -e 's/\.do//g')
