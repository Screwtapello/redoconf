#
# Tests for redoconf/util
#

. ../redoconf/util.sh

# Helper functions

assert_equal () {
	if [ -z "$3" ]
	then MSG="got: $(rc_shquote "$1") expected: $(rc_shquote "$2")"
	else MSG="$3"; fi

	[ "$1" = "$2" ] || die "$MSG"
}

assert_empty () {
	if [ -z "$2" ]
	then MSG="got: '$1'"
	else MSG="$2"; fi

	[ -z "$1" ] || die "$MSG"
}

#
# Tests for rc_log
#
log "hello" > stdout.tmp 2> stderr.tmp
STDOUT=$(cat stdout.tmp)
STDERR=$(cat stderr.tmp)
rm -f stdout.tmp stderr.tmp

assert_empty "$STDOUT" "log shouldn't write to stdout, got $STDOUT"
assert_equal "$STDERR" "hello" \
	"log wrote the wrong thing to stderr, got $STDERR"

#
# Tests for rc_shquote
#
_count_args () { echo $# ; }

# Make sure we can handle internal spaces and quotes.
INPUT="hello world with 'apostrophes' and \"quotes\""
QUOTED=$(rc_shquote $INPUT)

assert_equal "$(eval _count_args $QUOTED)" "1" \
	"rc_shquote produced broken output: $QUOTED"

# Make sure we can handle external spaces.
QUOTED=$(rc_shquote " spaces ")
assert_equal "$(eval printf "%s" "$QUOTED")" " spaces "

# Make sure we can handle leading and trailing apostrophes.
QUOTED=$(rc_shquote "'apostrophes'")
assert_equal "$(eval printf "%s" "$QUOTED")" "'apostrophes'"

#
# Tests for rc_abspath
#
REAL_BASEDIR=$(cd ..; echo "$PWD")

assert_equal "$(rc_abspath ../README)" "$REAL_BASEDIR/README"
assert_equal "$(rc_abspath .)" "$REAL_BASEDIR/tests/."
assert_equal "$(rc_abspath missing/path)" "$REAL_BASEDIR/tests/missing/path" \
	"rc_abspath can't handle missing paths"

#
# Tests for rc_filter_comments
#
cat > raw_file.tmp << EOF
# this is a comment, followed by a blank line

# And here's a line with spaces:
        
# And one with a tab:
	
line with a comment # here it is!
EOF
rc_filter_comments raw_file.tmp > filtered_file.tmp
read line < filtered_file.tmp

rm -f raw_file.tmp filtered_file.tmp

assert_equal "$line" "line with a comment" \
	"rc_filter_comments is broken: read $line"

#
# Tests for rc_map_get and rc_map_keys
#
cat > map_file.tmp << EOF
# Here's a line with a comment
foo bar
xyzzy plugh glorf # A comment on the end of this line too.
EOF

assert_equal "$(rc_map_get map_file.tmp xyzzy)" "plugh glorf"
assert_equal "$(rc_map_keys map_file.tmp)" "foo
xyzzy"

rm -f map_file.tmp

#
# Tests for rc_have_command
#
if ! rc_have_command "../redoconf/minimal-redo"; then
	die "rc_have_command can't detect executable files"
fi
if ! rc_have_command "export"; then
	die "rc_have_command can't detect shell builtins"
fi
if rc_have_command "not a real command"; then
	die "rc_have_command has false positives"
fi

#
# Tests for rc_append
#
commands=$(rc_append "greeting" "hello")

# If the variable is empty, no delimiter.
greeting=""
eval "$commands"
assert_equal "$greeting" "hello"

# If the variable has a value, add a space
greeting="bonjour"
eval "$commands"
assert_equal "$greeting" "bonjour hello"

#
# Tests for rc_append_with_delim
#
commands=$(rc_append_with_delim "greeting" " and " "everyone")

# If the variable is empty, no delimiter
greeting=""
eval "$commands"
assert_equal "$greeting" "everyone"

# If the variable is set, the delimiter should be added.
greeting="you, me"
eval "$commands"
assert_equal "$greeting" "you, me and everyone"

#
# Tests for rc_record
#

# Unset variables should be recorded as unset.
unset UNSET
assert_equal "$(rc_record "UNSET")" "unset UNSET"

# ...unless a value is povided on the command-line.
assert_equal "$(rc_record "UNSET=23")" "export UNSET='23'"

# ...unless that value is empty.
assert_equal "$(rc_record "UNSET=")" "unset UNSET"

# pkg-config is the only tool known to distinguisg between unset and empty
# environment variables, and "empty" makes it behave stupidly, so let's always
# unset.
export EMPTY=""
assert_equal "$(rc_record "EMPTY")" "unset EMPTY"

# Set variables should have their values read from the environment.
export VALUE="42"
assert_equal "$(rc_record "VALUE")" "export VALUE='42'"

# Explicitly providing a value should override the environment.
assert_equal "$(rc_record "VALUE=23")" "export VALUE='23'"

# Explicitly providing no value should override the environment.
assert_equal "$(rc_record "VALUE=")" "unset VALUE"

# Providing multiple variables and/or values should treat them individually.
export FOO="BAR"
assert_equal "$(rc_record "VALUE=23" "FOO" "EMPTY" "UNSET")" \
	"$(printf "export VALUE='23'\nexport FOO='BAR'\nunset EMPTY\nunset UNSET")"
